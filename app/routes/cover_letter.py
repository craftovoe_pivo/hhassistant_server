from fastapi import APIRouter
# from app import config
from app.model import CoverLetterArgs
from app.controller import LetterAssistant

cover_letter_router = APIRouter(prefix="/coverletter", tags=["CoverLetter"])

@cover_letter_router.post("", response_model=str)
def get_cover_letter(body: CoverLetterArgs) -> str:
    # TODO: Hardcoded
    cv_text = body.cv
    job_text = body.description

    # TODO: Hardcoded
    l_pr = 'prompt/letter_prompt.txt'
    c_j_pr = 'prompt/compress_job_prompt.txt'
    c_c_pr = 'prompt/compress_cv_prompt.txt'
    la = LetterAssistant(cv_text, job_text, l_pr,  c_j_pr, c_c_pr)
    return la.run()
