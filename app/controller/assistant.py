import openai
from app import config

openai.api_key = config['API_KEY']

class Assistant:
    def __init__(self, gpt_engine="text-davinci-003", max_tokens=1700, answer_path='answer.txt', sleeptime=10):
        self.gpt_engine = gpt_engine
        self.answer_path = answer_path
        self.max_tokens = max_tokens
        self.sleeptime = sleeptime
    
    def ask_gpt(self, question, context):
        completions = openai.Completion.create(
            engine = self.gpt_engine,
            prompt = context + question,
            max_tokens = self.max_tokens
        )
        message = completions.choices[0].text
        return message
