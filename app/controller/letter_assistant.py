from app.controller import Assistant

def read_file(path, lines=False):
    with open(path, "r") as file:
        if lines:
            return file.readlines()
        return ' '.join(file.readlines())

def append_record(rec, path, lines=False):
    with open(path, "w") as file:
        if lines:
            file.writelines(rec)
        else:
            file.write(rec)

class LetterAssistant(Assistant):
    def __init__(self, 
                 cv_txt, 
                 job_txt, 
                 letter_prompt_path, 
                 compress_job_prompt_path,
                 compress_cv_prompt_path):
        super().__init__()
        self.cv_txt = cv_txt
        self.job_txt = job_txt
        self.letter_prompt_txt = read_file(letter_prompt_path)
        self.compress_job_prompt = read_file(compress_job_prompt_path)
        self.compress_cv_prompt = read_file(compress_cv_prompt_path)

    def run(self):
        compressed_job = self.ask_gpt(self.compress_job_prompt, self.job_txt)
        compressed_cv = self.ask_gpt(self.compress_cv_prompt, self.cv_txt)
        append_record(compressed_job, 'compressed_job.txt')
        append_record(compressed_cv, 'compressed_cv.txt')
        context_txt = 'Информация о компании: {0}, информация о резюме пользователя: {1}'.format(compressed_job, compressed_cv)
        answer = self.ask_gpt(self.letter_prompt_txt, context_txt)
        return answer
        #append_record(answer, self.answer_path)