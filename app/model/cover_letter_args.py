from pydantic import BaseModel

class CoverLetterArgs(BaseModel):
    description: str
    cv: str
