Data Scientist
Печкин Игорь


Специализации:
Data Scientist
Аналитик

Занятость: полная занятость, частичная занятость, стажировка

График работы: полный день, гибкий график, удаленная работа

Опыт работы 2 года 9 месяцев
Сентябрь 2020 — по настоящее время
2 года 9 месяцев
Индивидуальное предпринимательство / частная практика / фриланс

Data Scientist.
1. Разработка NLP моделей c использованием BERT, облаков слов, TFIDF.
2. Разработка моделей для табличных данных на основе бустинга.
3. Анализ и визуализация данных.
4. Обработка и очистка данных.
5. Работа с базами данных Posgresql, SQLite.
6. Репетиторство, помощь в решении популярных онлайн курсов, разбор олимпиадных задач.

Опыт решения практических Data Science задач на python с использованием библиотек: TensorFlow, pytorch, scikit-learn, pandas, matplotlib, SciPy, xgboost, transformers, nltk. Понимание статистических методов. Знания и опыт применения алгоритмов машинного обучения. Опыт использования Linux. Знание GIT, SQL, Docker, Jira.

Умение работать в команде. Аналитический склад ума.
Способность быстро вникать в суть задачи.

Занял 4 место в хакатоне “Лидеры цифровой трансформации” от г. Москвы (Разработка классификатора для инновационных идей). Участвовал в хакатоне “MORE.tech 4.0” от ВТБ (создание системы подбора новостей для профильной ленты). Разработка робота-собаки с использованием компьютерного зрения и участие в конференции 
“Молодёжь и будущее авиации и космонавтики” организованной МАИ.

Высшее образование
2023
Московский авиационный институт (национальный исследовательский университет), Москва
Робототехнические и интеллектуальные системы

